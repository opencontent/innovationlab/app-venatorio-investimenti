import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PostsPageRoutingModule } from './posts-routing.module';

import { PostsPage } from './posts.page';
import { SecurePipe } from '../../pipe/secure';

@NgModule({
  imports: [CommonModule, FormsModule, IonicModule, PostsPageRoutingModule],
  declarations: [PostsPage, SecurePipe],
  exports: [SecurePipe],
})
export class PostsPageModule {}
