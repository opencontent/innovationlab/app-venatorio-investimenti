import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { tap } from 'rxjs/operators';
import { Observable, BehaviorSubject } from 'rxjs';

import { User } from './user';
import { AuthResponse } from './auth-response';
import { environment } from '../../environments/environment';
import { StorageService } from '../services/storage.service';
import jwt_decode from 'jwt-decode';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  authSubject = new BehaviorSubject(false);

  constructor(private httpClient: HttpClient, private storage: StorageService) {}

  login(user: User): Observable<AuthResponse> {
    return this.httpClient.post(`${environment.host}/api/sensor/auth`, user).pipe(
      tap(async (res: AuthResponse) => {
        if (res) {
          await this.storage.set(
            'USER',
            JSON.stringify({
              user: user,
            })
          );
          const decoded = jwt_decode(res['token']);
          console.log(decoded);
          await this.storage.set(
            'ACCESS_TOKEN',
            JSON.stringify({
              token: res['token'],
            })
          );
          await this.storage.set(
            'USER_ROLE',
            JSON.stringify({
              roles: decoded['https://hasura.io/jwt/claims']['x-hasura-allowed-roles'],
            })
          );
          await this.storage.set(
            'DATA_USER',
            JSON.stringify({
              data_user: decoded,
            })
          );
          await this.storage.set('EXPIRES_IN', decoded['exp']);
          this.authSubject.next(true);
        }
      })
    );
  }

  async logout() {
    await this.storage.clearStorage();
    this.authSubject.next(false);
  }

  async removeTokenb() {
    await this.storage.clearStorage();
    this.authSubject.next(false);
  }

  getToken() {
    return this.storage.getToken();
  }

  getUserRole() {
    return this.storage.getUserRole();
  }

  getCurrentUser(): Observable<any> {
    return this.httpClient.get(`${environment.host}/api/sensor/users/current`).pipe(tap(async (res: any) => {}));
  }

  updateUserProfile(user_data: any): Observable<any> {
    return this.httpClient
      .patch(`${environment.host}/api/sensor/users/current`, user_data)
      .pipe(tap(async (res: any) => {}));
  }
}
